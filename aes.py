'''
    File name: aes.py
    Author: Maks Górski
    Date created: 11/01/2020
    Date last modified: 11/01/2020
    Python Version: 3.6
    Purpose: .
'''

__all__ = ['AES']
__author__ = "Maks Górski"
__license__ = "Public domain"
__email__ = "maksymilian_gorski@wp.pl"

import rijndael_field